const express = require("express");
const router = express.Router();
const auth = require("../auth");
const userController = require("../controllers/user");


//Register
router.post("/register", (req, res) => 
{
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))

})


//LogIn
router.post("/login", (req, res) => 
{
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
})


//Update
router.put("/:userId/setAsAdmin", auth.verify, (req, res) => 
{
	const userData = auth.decode(req.headers.authorization)

	let data = 
	{
		userId: req.params.userId,
		isAdmin: userData.isAdmin
	}

	if (userData.email === "admin@email.com") 
	{

		if (userData.isAdmin === false) {
			userController.setAdmin(data).then(resultFromController => res.send(resultFromController))
		} else {
			res.send(`The user is already an Admin`)
		}
	} else {
		res.send(`Wrong user`);
	}
});


//Create Order
router.post("/checkout", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	if (userData.isAdmin == false) {
	const data = {
		userId: req.body.userId,
		productId: req.body.productId,
		totalAmount: req.body.totalAmount
	}

	userController.orderCheckout(data).then(resultFromController => res.send(resultFromController));

	} else {
		res.send(`Unauthorized User`)
	}
});

//Search all order as Admin
router.get("/orders", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	if (userData.isAdmin === true) {

		userController.getOrders().then(resultFromController => res.send(resultFromController))
	} else {
		res.send(`For Admin Use Only`)	
	}
})


//Search Users Order
router.get("/myorders", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	if (userData.isAdmin === false) {

		let data = userData

		userController.getUsersOrders(data).then(resultFromController => res.send(resultFromController))
	} else {
		res.send(`Unauthorized User`)	
	}

})


module.exports = router;
